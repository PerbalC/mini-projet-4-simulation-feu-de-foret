from tkinter import *
import argparse
import random

parser = argparse.ArgumentParser()
parser.add_argument("-rows",type =int,help="Renseigner combien de ligne dans votre foret",default=3)
parser.add_argument("-cols",type =int,help="Renseigner combien de colonne dans votre foret",default=3)
parser.add_argument("-cell_size",type =int,help="Renseigner la taille de vos cases dans votre foret",default=20)
parser.add_argument("-afforestation",type=float,help="Resneigner le pourcentage de chance qu'une case soit un arbre",default=.6)
args = parser.parse_args()


def creation_map(row,col,size,proba):


    x1 = 0
    y1 = 0
    x2 =size
    y2=size
    nb_col=0
    int_x1 = x1
    int_x2 = x2
    
    while nb_col < col:
                int_x1 += size
                int_x2 += size
                int_y1 = y1
                int_y2 = y2
                nb_row =0
                while nb_row < row:
                    random_number = random.random()
                    if random_number <= proba:
                        color = "green"
                    else:
                        color="white"
                    if nb_row == 0:
                        canvas.create_rectangle(int_x1 ,int_y1 ,int_x2 ,int_y2 ,fill=color,tag="rectangleHaut")
                    elif nb_row == row -1:
                        canvas.create_rectangle(int_x1 ,int_y1 ,int_x2 ,int_y2 ,fill=color,tag="rectangleBas")
                    else:
                        canvas.create_rectangle(int_x1 ,int_y1 ,int_x2 ,int_y2 ,fill=color,tag="rectangle")
                    int_y1 += size
                    int_y2 += size
                    nb_row +=1
                nb_col = nb_col +1
    canvas.pack()

def regener():
    creation_map(args.rows,args.cols,args.cell_size,args.afforestation)
    
def toutCramer():
    canvas.bind("<Button-1>", callbackFlame)
def toutPlanter():
    canvas.bind("<Button-1>",callbackPlante)
def callbackFlame(event):
    canvas = event.widget
    item = canvas.find_closest(event.x,event.y)
    canvas.itemconfig(item[0],fill='red')

def callbackPlante(event):
    canvas = event.widget
    item = canvas.find_closest(event.x,event.y)
    canvas.itemconfig(item[0],fill='green')

def simulation_regle1():
    feu = []
    cendre = []
    for rectangle in canvas.find_all():
        if canvas.itemcget(rectangle,'fill') == 'red':
            feu.append(rectangle)
        if canvas.itemcget(rectangle,'fill') == 'gray':
            cendre.append(rectangle)
    for rectangle in feu:
        tag = canvas.gettags(rectangle)
        if  tag[0] == 'rectangleBas':
            #Droite
            if canvas.itemcget(rectangle+args.rows,'fill') == 'green':
                canvas.itemconfig(rectangle+args.rows, fill='red')
            #Gauche
            if canvas.itemcget(rectangle-args.rows,'fill') == 'green':
                canvas.itemconfig(rectangle-args.rows, fill='red')
            #Haut
            if canvas.itemcget(rectangle-1,'fill') == 'green':
                canvas.itemconfig(rectangle-1, fill='red')
        elif tag[0] == 'rectangleHaut':
            #Droite
            if canvas.itemcget(rectangle+args.rows,'fill') == 'green':
                canvas.itemconfig(rectangle+args.rows, fill='red')
            #Gauche
            if canvas.itemcget(rectangle-args.rows,'fill') == 'green':
                canvas.itemconfig(rectangle-args.rows, fill='red')
            #Bas
            if canvas.itemcget(rectangle+1,'fill') == 'green':
                canvas.itemconfig(rectangle + 1, fill='red')
        else:
            #Droite
            if canvas.itemcget(rectangle+args.rows,'fill') == 'green':
                canvas.itemconfig(rectangle+args.rows, fill='red')
            #Gauche
            if canvas.itemcget(rectangle-args.rows,'fill') == 'green':
                canvas.itemconfig(rectangle-args.rows, fill='red')
            #Bas
            if canvas.itemcget(rectangle+1,'fill') == 'green':
                canvas.itemconfig(rectangle + 1, fill='red')
            #Haut
            if canvas.itemcget(rectangle-1,'fill') == 'green':
                canvas.itemconfig(rectangle-1, fill='red')
        canvas.itemconfig(rectangle,fill='gray')
    for rectangle in cendre:
        canvas.itemconfig(rectangle,fill='white')
    master.after(1000,simulation_regle1)    

def selectregle1():
    button_simulation.config(command=simulation_regle1)

def simulation_regle2():
    arbre = []
    aEnflammer = []
    feu=[]
    cendre = []
    for rectangle in canvas.find_all():
        if canvas.itemcget(rectangle,'fill') == 'green':
            arbre.append(rectangle)
        if canvas.itemcget(rectangle,'fill') =='red':
            feu.append(rectangle)
        if canvas.itemcget(rectangle,'fill') == 'gray':
            cendre.append(rectangle)
    for rectangle in arbre:
        tag = canvas.gettags(rectangle)
        voisin = []
        if  tag[0] == 'rectangleBas':
            #Droite
            if canvas.itemcget(rectangle+args.rows,'fill') == 'red':
                voisin.append(rectangle+args.rows)
            #Gauche
            if canvas.itemcget(rectangle-args.rows,'fill') == 'red':
                voisin.append(rectangle-args.rows)
            #Haut
            if canvas.itemcget(rectangle-1,'fill') == 'red':
                voisin.append(rectangle-1)
        elif tag[0] == 'rectangleHaut':
            #Droite
            if canvas.itemcget(rectangle+args.rows,'fill') == 'red':
                voisin.append(rectangle+args.rows)
            #Gauche
            if canvas.itemcget(rectangle-args.rows,'fill') == 'red':
                voisin.append(rectangle-args.rows)
            #Bas
            if canvas.itemcget(rectangle+1,'fill') == 'red':
                voisin.append(rectangle+1)
        else:
            #Droite
            if canvas.itemcget(rectangle+args.rows,'fill') == 'red':
                voisin.append(rectangle+args.rows)
            #Gauche
            if canvas.itemcget(rectangle-args.rows,'fill') == 'red':
                voisin.append(rectangle-args.rows)
            #Bas
            if canvas.itemcget(rectangle+1,'fill') == 'red':
                voisin.append(rectangle+1)
            #Haut
            if canvas.itemcget(rectangle-1,'fill') == 'red':
                voisin.append(rectangle-1)
        if random.random() <= (1- (1/(len(voisin)+1))):
            aEnflammer.append(rectangle)  
    for rectangle in aEnflammer:
        canvas.itemconfig(rectangle,fill='red')   
    for rectangle in feu:
        canvas.itemconfig(rectangle,fill='gray')
    for rectangle in cendre:
        canvas.itemconfig(rectangle,fill='white')
    master.after(1000,simulation_regle2)

def selectregle2():
    button_simulation.config(command=simulation_regle2)
    
master = Tk()
master.title("Simulation d'un feu de forêt")

canvas = Canvas(master,height=(args.rows+1)*args.cell_size,width=(args.cols+1)*args.cell_size)

menu = Tk()
menu.title("Menu de la simulation")
menu.rowconfigure(0,weight=1,minsize=15)
menu.rowconfigure(2,weight=1,minsize=15)
menu.rowconfigure(4,weight=1,minsize=15)


button_enflammer = Button(menu, text="Enflammer",background='red',command=toutCramer)
button_enflammer.grid(column=0,row=1)

button_planter = Button(menu, text="Planter",background='green',command=toutPlanter)
button_planter.grid(column=2,row=1)

button_regenerer = Button(menu, text="Regenerer",command=regener)
button_regenerer.grid(column=4,row=1)
button_simulation = Button(menu,text="Lancez la simulation")
button_simulation.grid(column=2,row=3)



radio_button_regle1 = Radiobutton(menu,text="Regle 1",value=1,command=selectregle1)
radio_button_regle1.grid(column=0,row=5)
radio_button_regle2 = Radiobutton(menu,text="Regle 2",value=2,command=selectregle2)
radio_button_regle2.grid(column=2,row=5)

creation_map(args.rows,args.cols,args.cell_size,args.afforestation)

menu.mainloop()
master.mainloop()